<?php

class Task extends Eloquent {

    protected $fillable = ['name', 'description'];

}
